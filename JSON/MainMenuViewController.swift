//
//  MainMenuController.swift
//  JSON
//
//  Created by Денис Мурлатовский on 19.06.16.
//  Copyright © 2016 Денис Мурлатовский. All rights reserved.
//


import UIKit
import SwiftyJSON


class MainMenuViewController: UITableViewController {
    
    var MainMenuArray = [String]()
    var NamesStationsArray = [StationsTable]()
    
    
   

    

    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        
        
        
        
        MainMenuArray = ["Откуда едем","Куда едем"]
        
        NamesStationsArray =
            [StationsTable(StationsTitle: parseJsonStations("citiesFrom",stations: "stations",stationTitle: "stationTitle")),
             StationsTable(StationsTitle: parseJsonStations("citiesTo", stations: "stations",stationTitle: "stationTitle"))]
        
        
    }
    
    
    func parseJsonStations(citiesAction: String, stations: String, stationTitle: String) ->[String] {
        
        let path : String = NSBundle.mainBundle().pathForResource("allStations", ofType: "json") as String!
        let jsonDate = NSData(contentsOfFile: path) as NSData!
        let readbleJson = JSON(data: jsonDate, options: NSJSONReadingOptions.MutableContainers, error: nil)
        
        var stationsArray = [String]()
        
        
        for  i in 0...readbleJson[citiesAction].count {
            
            for  k in 0..<readbleJson[citiesAction][i][stations].count{
                
                
                if readbleJson[citiesAction][i][stations][k][stationTitle] != nil  {
                    
                    
                    stationsArray.append(readbleJson[citiesAction][i][stations][k][stationTitle].string as String!)
                
                   
                    
                }
                
            }
           
            
        }
        return stationsArray
    }
    
    

    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return MainMenuArray.count
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        
        var Cell = self.tableView.dequeueReusableCellWithIdentifier("MainCell", forIndexPath: indexPath) as UITableViewCell
        
        Cell.textLabel?.text = MainMenuArray[indexPath.row]
        return Cell
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        var indexPath : NSIndexPath = self.tableView.indexPathForSelectedRow!
        
        var DestViewController = segue.destinationViewController as! StationsMenuViewController
        
        var StationsTableArrayTwo : StationsTable
        
        StationsTableArrayTwo = NamesStationsArray[indexPath.row]
        
        
        DestViewController.StationsMenuArray = StationsTableArrayTwo.StationsTitle
        
        
              
        
    
        
        
 
    }
    
    
    
}
