//
//  StationsMenuController.swift
//  JSON
//
//  Created by Денис Мурлатовский on 19.06.16.
//  Copyright © 2016 Денис Мурлатовский. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON




class StationsMenuViewController:  UITableViewController{
    
    var StationsMenuArray = [String]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
 
    
    }
    
    
    
    
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return StationsMenuArray.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var Cell = self.tableView.dequeueReusableCellWithIdentifier("StationsCell", forIndexPath: indexPath) as UITableViewCell
        
        Cell.textLabel?.text = StationsMenuArray[indexPath.row]
        return Cell
    }
    

    
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        
        
    }
    
}
